﻿using PLC.Codegen;
using PLC.Nodes;
using PLC.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = Node.Create<Rootnode>();
            var solver = new Nodesolver(root);

            var no = Node.Create<NONode>();

            var no1 = Node.Create<NONode>();
            var no2 = Node.Create<NONode>();

            
            var output = Node.Create<NONode>();
            no1.AddChild(output);
            no2.SetNodeValue(true);

            no.NodeLabel = NodeLabels.X3;
            no1.NodeLabel = NodeLabels.X1;
            no2.NodeLabel = NodeLabels.X0;
            output.NodeLabel = NodeLabels.Y0;

            no.AddChild(no1);
            no.AddChild(no2);
            no.SetNodeValue(true);

            root.AddChild(no);
            var gen = new ProgramGenerator(new[] { root });
            gen.Generate();
            var result = solver.Solve();

            var outp = result.GetOutputs.Any(x => x.NodeOutput);

            var outputResult = result.GetOutputs.FirstOrDefault().NodeOutput;

            var pf = ProgramFile.Serialize(ProgramFile.Create());

            File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), @"program.json"), pf);
        }
    }
}
