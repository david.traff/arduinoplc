﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public sealed class SolverResult
    {
        private ReadOnlyCollection<SolvedNode> _Nodes;

        public ReadOnlyCollection<SolvedNode> Nodes => _Nodes;

        private SolvedBranch _Rootbranch;
        public SolvedBranch Rootbranch => _Rootbranch;

        public SolverResult(IList<SolvedNode> nodes, SolvedBranch rootbranch)
        {
            _Nodes = new ReadOnlyCollection<SolvedNode>(nodes);
            _Rootbranch = rootbranch;
        }

        public IEnumerable<SolvedNode> GetOutputs => _Nodes.Where(x => Node.IsOutputNode(x.Node));
    }
}
