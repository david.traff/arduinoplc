﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace PLC
{
    public class Nodefactory : SingleInstance<Nodefactory>
    {
        private HashSet<Node> _Nodes = new HashSet<Node>();

        internal IEnumerable<Node> Nodes => _Nodes;

        private readonly BindingFlags _Flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;

        public T CreateNode<T>() where T : Node => CreateNode<T>(Guid.Empty, null);

        public T CreateNode<T>(Guid id, IEnumerable<Guid> parentId) where T : Node
        {
            var type = typeof(T);
            var constructor = type.GetConstructor(_Flags, null, new[] { typeof(Guid), typeof(IEnumerable<Guid>) }, null);

            T node;
            if (constructor == null)
            {
                node = (T)Activator.CreateInstance(type, _Flags, null, new object[] { id == Guid.Empty ? Guid.NewGuid() : id }, null);
            }
            else
            {
                node = (T)Activator.CreateInstance(type, _Flags, null, new object[] { id == Guid.Empty ? Guid.NewGuid() : id, parentId ?? new List<Guid>() }, null);
            }

            _Nodes.Add(node);

            return node;
        }

        public bool DeleteNode(Node node)
        {
            if (node == null)
                return false;

            return _Nodes.Remove(node);
        }

        public bool DeleteNode(Guid id) => DeleteNode(_Nodes.FirstOrDefault(x => x.Id == id));

        public void RequestUpdate()
        {

        }
    }
}
