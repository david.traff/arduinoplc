﻿using PLC.Codegen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public class Nodesolver
    {
        private Rootnode _Rootnode;
        public Rootnode Rootnode => _Rootnode;

        private List<SolvedNode> _Result;
        private bool _HasOutputNode = false;

        public Nodesolver(Rootnode rootnode)
        {
            _Rootnode = rootnode;
        }

        public SolverResult Solve()
        {
            _Result = new List<SolvedNode>();
            _HasOutputNode = false;
            var rootbranch = Traverse(true, _Rootnode);

            if (_HasOutputNode == false)
                throw new Exception($"No output found in the solve");

            return new SolverResult(_Result, rootbranch);
        }

        private SolvedBranch Traverse(bool nodeInput, Node node)
        {
            var solved = GetSolved(nodeInput, node);
            var childBranches = new List<SolvedBranch>();
            _Result.Add(solved);

            if (Node.IsOutputNode(node))
            {
                _HasOutputNode = true;

                if (node.HasChildren)
                    throw new Exception("Outputs cannot have children. Only siblings");
            }

            if (node.NodeLabel == NodeLabels.None)
                throw new Exception($"Node {node.Id} has no label.");

            //since siblings are OR-gates, and we don't know which of this node's children which holds the next branch
            //we need to know if we should send it forward.
            //thus the solved.NodeOutput || anySiblingIsActivated
            //will maybe do parentId as a list in the future to make it less hacky.

            var anySiblingIsActivated = node.Siblings.Any(x => x.GetOutput(nodeInput));

            foreach(var child in node.Children)
            {
                childBranches.Add(Traverse(solved.NodeOutput || anySiblingIsActivated, child));
            }

            return new SolvedBranch(node, solved.NodeInput, solved.NodeValue, solved.NodeOutput, childBranches);
        }

        private SolvedNode GetSolved(bool nodeInput, Node node)
        {
            var nodeOutput = node.GetOutput(nodeInput);

            return new SolvedNode(node, nodeInput, node.NodeValue, nodeOutput);
        }
    }
}
