﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class DebuggerGenerator : ICodeGenerator
    {
        public string ReplaceToken => "debugger";

        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            return string.Format("{0}Serial.write({1}, {2});", GenUtils.GetIndent(1), DeclarationGenerator.DatavariableName, variablemap.Count);
        }
    }
}
