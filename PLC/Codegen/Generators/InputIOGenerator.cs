﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class InputIOGenerator : ICodeGenerator
    {
        public string ReplaceToken => "inputio";

        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            var sb = new StringBuilder();

            foreach(var node in generator.AllNodes.Where(x => Node.IsInputNode(x)))
            {
                sb.AppendLine(GenUtils.GetIndent(1));
                sb.AppendFormat("{0}readPin({1}, {2});", GenUtils.GetIndent(1), ProgramGenerator.GetPinNumber(node.NodeLabel), variablemap[node.NodeLabel]);
            }

            return sb.ToString();
        }
    }
}
