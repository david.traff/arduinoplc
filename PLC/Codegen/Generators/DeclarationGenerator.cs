﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class DeclarationGenerator : ICodeGenerator
    {
        public string ReplaceToken => "declaration";

        public const string DatavariableName = "data";


        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            return string.Format("bool {0}[{1}];", DatavariableName, variablemap.Count);
        }
    }
}
