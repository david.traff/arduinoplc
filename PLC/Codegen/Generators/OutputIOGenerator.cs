﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class OutputIOGenerator : ICodeGenerator
    {
        public string ReplaceToken => "outputio";

        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            var sb = new StringBuilder();

            foreach(var output in generator.AllNodes.Where(x => Node.IsOutputNode(x)))
            {
                var pinNumber = ProgramGenerator.GetPinNumber(output.NodeLabel);
                var pinOutput = string.Format("{0}[{1}] ? HIGH : LOW", DeclarationGenerator.DatavariableName, variablemap[output.NodeLabel]);

                sb.AppendFormat("{0}digitalWrite({1}, {2});", GenUtils.GetIndent(1), pinNumber, pinOutput);
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
