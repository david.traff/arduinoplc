﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class LogicGenerator : ICodeGenerator
    {
        public string ReplaceToken => "logic";

        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            var sb = new StringBuilder();

            foreach(var root in generator.SolvedResults)
            {
                var outputStr = GenUtils.GetIndent(1);
                foreach(var output in root.GetOutputs)
                {
                    outputStr += string.Format("{0}[{1}] = ", DeclarationGenerator.DatavariableName, variablemap[output.Node.NodeLabel]);
                }

                var exp = GenerateExpression(root.Rootbranch.Node, variablemap);

                sb.AppendFormat("{0}{1};", outputStr, exp);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private string GenerateExpression(Node node, Variablemap variablemap)
        {
            var nodeName = string.Format("{0}[{1}]", DeclarationGenerator.DatavariableName, variablemap[node.NodeLabel]);

            if (!node.HasChildren || node.Children.Any(x => Node.IsOutputNode(x)))
                return nodeName;

            var childCount = node.Children.Count();
            var childData = childCount > 1 ? "(" : "";

            for (int i = 0; i < childCount; i++)
            {
                var isLastChild = childCount - 1 == i;
                var child = node.Children.ElementAt(i);

                childData += GenerateExpression(child, variablemap) + (isLastChild ? "" : " || ");
            }

            if (childCount > 1)
                childData += ")";

            return nodeName + " && " + childData;
        }
    }
}
