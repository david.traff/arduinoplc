﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen.Generators
{
    public class PinSetupGenerator : ICodeGenerator
    {
        public string ReplaceToken => "pinsetup";

        public string Generate(ProgramGenerator generator, Variablemap variablemap)
        {
            var sb = new StringBuilder();

            GenerateNodes(sb, generator.AllNodes.Where(x => Node.IsInputNode(x)), variablemap, false);
            GenerateNodes(sb, generator.AllNodes.Where(x => Node.IsOutputNode(x)), variablemap, true);

            return sb.ToString();
        }

        private void GenerateNodes(StringBuilder sb, IEnumerable<Node> nodes, Variablemap varmap, bool isOutput)
        {
            foreach (var input in nodes)
            {
                var pinNr = ProgramGenerator.GetPinNumber(input.NodeLabel);

                sb.AppendFormat("{0}pinMode({1}, {2});", GenUtils.GetIndent(1), pinNr, isOutput ? "OUTPUT" : "INPUT");
                sb.AppendFormat("//pin: {0} = {1}, data-index = {2}", pinNr, input.NodeLabel, varmap[input.NodeLabel]);
                sb.AppendLine();
            }
        }
    }
}
