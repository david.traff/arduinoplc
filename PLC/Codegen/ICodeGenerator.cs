﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen
{
    public interface ICodeGenerator
    {
        string ReplaceToken { get; }
        string Generate(ProgramGenerator generator, Variablemap variablemap);
    }
}
