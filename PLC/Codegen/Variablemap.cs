﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen
{
    /// <summary>
    /// Represents an index-map of a nodelabel.
    /// </summary>
    public sealed class Variablemap
    {
        private Dictionary<NodeLabels, int> _VariableMap = new Dictionary<NodeLabels, int>();

        public Variablemap(IEnumerable<Node> nodes)
        {
            foreach(var node in nodes)
            {
                if (!_VariableMap.ContainsKey(node.NodeLabel))
                    _VariableMap.Add(node.NodeLabel, _VariableMap.Count);
            }
        }

        public int this[NodeLabels key]
        {
            get
            {
                if (_VariableMap.TryGetValue(key, out int value))
                    return value;

                return -1;
            }
            set
            {
                if (_VariableMap.ContainsKey(key))
                    _VariableMap[key] = value;
                else
                    _VariableMap.Add(key, value);
            }
        }

        public int Count => _VariableMap.Count;
    }
}
