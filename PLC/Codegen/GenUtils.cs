﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen
{
    public static class GenUtils
    {
        private const string Indent = "    ";

        public static string GetIndent(int indentLevel)
        {
            var output = "";
            for (int i = 0; i < indentLevel; i++)
            {
                output += Indent;
            }
            return output;
        }
    }
}
