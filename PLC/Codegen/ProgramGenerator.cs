﻿using PLC.Codegen.Generators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Codegen
{
    public class ProgramGenerator
    {
        private IEnumerable<Rootnode> _RootNodes;
        public IEnumerable<Rootnode> RootNodes => _RootNodes;

        private IEnumerable<SolverResult> _SolvedResults;
        public IEnumerable<SolverResult> SolvedResults => _SolvedResults;

        public IEnumerable<Node> AllNodes => _SolvedResults.SelectMany(x => x.Nodes.Select(k => k.Node));

        private static Dictionary<NodeLabels, int> PinMap = new Dictionary<NodeLabels, int>
        {
            {NodeLabels.X0, 3},
            {NodeLabels.X1, 4},
            {NodeLabels.X2, 5},
            {NodeLabels.X3, 6},

            {NodeLabels.Y0, 7},
            {NodeLabels.Y1, 8},
            {NodeLabels.Y2, 9},
            {NodeLabels.Y3, 10}
        };

        private static readonly ICodeGenerator[] Generators = new ICodeGenerator[]
        {
            new DeclarationGenerator(),
            new InputIOGenerator(),
            new LogicGenerator(),
            new OutputIOGenerator(),
            new PinSetupGenerator(),
            new DebuggerGenerator()
        };

        public static int GetPinNumber(NodeLabels node) => PinMap[node];

        public ProgramGenerator(IEnumerable<Rootnode> nodes)
        {
            _RootNodes = nodes;
        }

        public void Generate()
        {
            SolveRootnodes();

            var template = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory() + @"\template.c"));
            var variableMap = new Variablemap(AllNodes);
            
            foreach(var generator in Generators)
            {
                var output = generator.Generate(this, variableMap);

                template = template.Replace("/*<<" + generator.ReplaceToken + ">>*/", output);
            }

            File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory() + @"\output.c"), template);
        }

        private void SolveRootnodes()
        {
            var solved = new List<SolverResult>();

            foreach(var rootnode in _RootNodes)
            {
                var solver = new Nodesolver(rootnode);

                solved.Add(solver.Solve());
            }

            _SolvedResults = solved;
        }
    }
}
