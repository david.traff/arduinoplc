﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public abstract class Node : IEquatable<Node>
    {
        /// <summary>
        /// Represents the <see cref="Node.ParentId"/> if the node has no parent.
        /// </summary>
        public static readonly Guid ParentlessId = Guid.Empty;

        internal Node(Guid id) => _Id = id;

        internal Node(Guid id, IEnumerable<Guid> parentIds)
        {
            _Id = id;
            _ParentIds = parentIds.ToList();
        }

        private Guid _Id;
        public Guid Id => _Id;

        private List<Guid> _ParentIds = new List<Guid>();
        public IEnumerable<Guid> ParentIds => _ParentIds;

        private bool _NodeValue;
        public bool NodeValue => _NodeValue;

        public NodeLabels NodeLabel { get; set; } = NodeLabels.None;

        protected abstract bool GetNodeValue(bool input);

        public IEnumerable<Node> Children => Nodefactory.Instance.Nodes.Where(x => x.IsChildOf(this));

        public IEnumerable<Node> Parents => _ParentIds.Select(x => Nodefactory.Instance.Nodes.FirstOrDefault(k => k.Id == x));

        public IEnumerable<Node> Siblings => Nodefactory.Instance.Nodes.Where(x => IsSiblingOf(x));

        public bool HasChildren => Nodefactory.Instance.Nodes.Any(x => x.ParentIds.Any(k => k == this.Id));


        public bool IsSiblingOf(Node sibling)
        {
            foreach(var parent in _ParentIds)
            {
                if (!sibling.IsChildOf(parent))
                    return false;
            }

            return true;
        }

        public bool IsChildOf(Node parent) => IsChildOf(parent.Id);

        public bool IsChildOf(Guid parentId) => _ParentIds.Any(x => x == parentId);

        public bool GetOutput(bool input)
        {
            //always set the node-value to true since an output-node doesn't have any Value, only outputvalue
            if (IsOutputNode(this))
                return input && GetNodeValue(true);

            return input && GetNodeValue(_NodeValue);
        }

        public void SetNodeValue(bool value)
        {
            this._NodeValue = value;
            Nodefactory.Instance.RequestUpdate();
        }

        public void AddChild(Node node)
        {
            if (IsOutputNode(this) && !IsOutputNode(node))
                throw new InvalidOperationException($"Cannot add an input-node as a child to an output-node. (input-id: {node.Id}, output-id: {this.Id})");

            //return if it's already a parent.
            if (node.ParentIds.Any(x => x == this.Id))
                return;

            node._ParentIds.Add(this.Id);
            Nodefactory.Instance.RequestUpdate();
        }

        public void RemoveChild(Node node)
        {
            node._ParentIds.Remove(this.Id);
            Nodefactory.Instance.RequestUpdate();
        }

        public bool Equals(Node other) => this.Id == other.Id;

        public static T Create<T>() where T : Node => Nodefactory.Instance.CreateNode<T>();

        public static T Create<T>(Guid id, IEnumerable<Guid> parentIds) where T : Node => Nodefactory.Instance.CreateNode<T>(id, parentIds);



        public static bool IsInputNode(Node node) => IsInputNode(node.NodeLabel);
        public static bool IsInputNode(NodeLabels label)
        {
            var labelValue = (int)label;

            return labelValue >= (int)NodeLabels.X0 && labelValue <= (int)NodeLabels.X63;
        }

        public static bool IsOutputNode(Node node) => IsOutputNode(node.NodeLabel);
        public static bool IsOutputNode(NodeLabels label)
        {
            var labelValue = (int)label;

            return labelValue >= (int)NodeLabels.Y0 && labelValue <= (int)NodeLabels.Y63;
        }

        public static bool IsMemoryNode(Node node) => IsMemoryNode(node.NodeLabel);
        public static bool IsMemoryNode(NodeLabels label)
        {
            var labelValue = (int)label;

            return labelValue >= (int)NodeLabels.M0 && labelValue <= (int)NodeLabels.M63;
        }

        public static bool IsTimerNode(Node node) => IsTimerNode(node.NodeLabel);
        public static bool IsTimerNode(NodeLabels label)
        {
            var labelValue = (int)label;

            return labelValue >= (int)NodeLabels.T0 && labelValue <= (int)NodeLabels.T63;
        }
    }
}
