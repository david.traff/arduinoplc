﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    /// <summary>
    /// Represents a solved state-node.
    /// </summary>
    public class SolvedNode
    {
        private Node _Node;
        public Node Node => _Node;

        private bool _NodeInput;
        public bool NodeInput => _NodeInput;

        private bool _NodeValue;
        public bool NodeValue => _NodeValue;

        private bool _NodeOutput;
        public bool NodeOutput => _NodeOutput;

        public SolvedNode(Node node, bool nodeInput, bool nodeValue, bool nodeOutput)
        {
            _Node = node;
            _NodeInput = nodeInput;
            _NodeValue = nodeValue;
            _NodeOutput = nodeOutput;
        }
    }
}
