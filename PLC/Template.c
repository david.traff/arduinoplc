﻿/*<<declaration>>*/

void setup() {
	// put your setup code here, to run once:
	Serial.begin(9600);
/*<<pinsetup>>*/
}

void loop() {
	// put your main code here, to run repeatedly:
/*<<inputio>>*/

/*<<logic>>*/

/*<<outputio>>*/

/*<<debugger>>*/
}

void readPin(int pin, int dataIdx) {
	data[dataIdx] = digitalRead(pin) == HIGH;
}