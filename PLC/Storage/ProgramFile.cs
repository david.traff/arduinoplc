﻿using Newtonsoft.Json;
using PLC.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Storage
{
    /// <summary>
    /// Represents a stored PLC-program
    /// </summary>
    public class ProgramFile
    {
        public IEnumerable<StoredNode> Nodes { get; set; }

        public IEnumerable<Rootnode> GetRootnodes()
        {
            return Nodes
                .Select(x => x.Create())
                .Where(x => x is Rootnode)
                .Select(x => x as Rootnode);
        }

        public static ProgramFile Create()
        {
            var nodes = new List<StoredNode>();

            foreach(var node in Nodefactory.Instance.Nodes)
            {
                var isInverted = node is NCNode || node is NONode;

                var stored = new StoredNode()
                {
                    Id = node.Id,
                    ParentIds = node.ParentIds,
                    IsInverted = isInverted,
                    NodeLabel = node.NodeLabel.ToString()
                };

                nodes.Add(stored);
            }

            return new ProgramFile()
            {
                Nodes = nodes
            };
        }


        public static string Serialize(ProgramFile file) => JsonConvert.SerializeObject(file);

        public static ProgramFile Deserialize(string data) => JsonConvert.DeserializeObject<ProgramFile>(data);
    }
}
