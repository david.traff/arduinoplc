﻿using Newtonsoft.Json;
using PLC.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Storage
{
    public sealed class StoredNode
    {
        public Guid Id { get; set; }

        public IEnumerable<Guid> ParentIds { get; set; }

        public bool IsInverted { get; set; }

        [JsonIgnore]
        public NodeLabels Nodelabel
        {
            get
            {
                if (string.IsNullOrEmpty(NodeLabel))
                    return NodeLabels.None;

                try
                {
                    return (NodeLabels)Enum.Parse(typeof(NodeLabels), NodeLabel);
                }
                catch (Exception)
                {
                    return NodeLabels.None;
                }
            }
        }

        public string NodeLabel { get; set; }

        public Node Create()
        {
            Node node = null;

            if (Node.IsInputNode(Nodelabel) || Node.IsOutputNode(Nodelabel))
                node = IsInverted ? (Node)Node.Create<NCNode>(Id, ParentIds) : Node.Create<NONode>(Id, ParentIds);
            else if (Nodelabel == NodeLabels.RootNode)
                node = Node.Create<Rootnode>();

            return node;
        }
    }
}
