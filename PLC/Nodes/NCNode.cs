﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Nodes
{
    /// <summary>
    /// Normally closed node.
    /// </summary>
    public sealed class NCNode : Node
    {
        internal NCNode(Guid id) : base(id)
        {
        }

        internal NCNode(Guid id, IEnumerable<Guid> parentIds) : base(id, parentIds)
        {

        }

        protected override bool GetNodeValue(bool input) => !input;
    }
}
