﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Nodes
{
    /// <summary>
    /// Normally open node.
    /// </summary>
    public sealed class NONode : Node
    {
        internal NONode(Guid id) : base(id)
        {
        }

        internal NONode(Guid id, IEnumerable<Guid> parentIds) : base(id, parentIds)
        {

        }

        protected override bool GetNodeValue(bool input) => input;
    }
}
