﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public sealed class SolvedBranch : SolvedNode
    {
        public SolvedBranch(Node node, bool nodeInput, bool nodeValue, bool nodeOutput, IList<SolvedBranch> children) : base(node, nodeInput, nodeValue, nodeOutput)
        {
            _Children = new ReadOnlyCollection<SolvedBranch>(children);
        }

        private ReadOnlyCollection<SolvedBranch> _Children;

        public ReadOnlyCollection<SolvedBranch> Children => _Children;
    }
}
