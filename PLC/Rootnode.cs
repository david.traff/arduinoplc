﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public sealed class Rootnode : Node
    {
        internal Rootnode(Guid id) : base(id)
        {
            this.NodeLabel = NodeLabels.RootNode;
        }

        protected override bool GetNodeValue(bool input) => true;
    }
}
