﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public abstract class SingleInstance<T>
    {
        private static object _Lock = new object();
        private static T _Instance;
        public static T Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock(_Lock)
                    {
                        if (_Instance == null)
                            _Instance = (T)Activator.CreateInstance(typeof(T));
                    }
                }

                return _Instance;
            }
        }
    }
}
