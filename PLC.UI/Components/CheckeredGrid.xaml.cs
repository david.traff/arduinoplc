﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PLC.UI.Components
{
    /// <summary>
    /// Interaction logic for CheckeredGrid.xaml
    /// </summary>
    public partial class CheckeredGrid : UserControl
    {
        public static readonly DependencyProperty SquareSizeProperty = DependencyProperty.Register("SquareSize", typeof(int), typeof(CheckeredGrid), new UIPropertyMetadata(150));

        private static SolidColorBrush DarkColor = new SolidColorBrush(Color.FromArgb(255, 245, 245, 245));
        private static SolidColorBrush LightColor = new SolidColorBrush(Color.FromArgb(255, 235, 235, 235));

        public int SquareSize
        {
            get
            {
                return (int)GetValue(SquareSizeProperty);
            }
            set
            {
                SetValue(SquareSizeProperty, value);
            }
        }



        public CheckeredGrid()
        {
            var parent = this.Parent;
            InitializeComponent();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            var squareSize = SquareSize;
            
            for(var x = 0; x < (this.ActualWidth / squareSize); x++)
            {
                for(var y = 0; y < (this.ActualHeight / squareSize); y++)
                {
                    var isEven = ((y % 2 == 0) ? x : x + 1) % 2 == 0;

                    drawingContext.DrawRectangle(isEven ? DarkColor : LightColor, null, new Rect(x * squareSize, y * squareSize, squareSize, squareSize));
                }
            }
            
        }
    }
}
