﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PLC.UI.Components
{
    public abstract class DraggableComponent : ComponentBase
    {
        public delegate void DraggedEventHandler(object sender, DraggedEventArgs e);
        public event DraggedEventHandler OnDrag;

        private double _X;
        public double X
        {
            get
            {
                return _X;
            }
            set
            {
                if (Snap)
                    _X = Math.Floor(value / SnapSize) * SnapSize;
                else
                    _X = value;

                Notify(nameof(X));
            }
        }

        private double _Y;
        public double Y
        {
            get
            {
                return _Y;
            }
            set
            {
                if (Snap)
                    _Y = Math.Floor(value / SnapSize) * SnapSize;
                else
                    _Y = value;

                Notify(nameof(Y));
            }
        }

        public bool Snap { get; set; } = true;

        public double SnapSize { get; set; } = 25;

        private bool _IsDragging = false;
        public bool IsDragging => _IsDragging;

        private Panel _Parent;
        private Point _StartPoint;

        public Node Node { get; set; }

        public DraggableComponent()
        {
            this.MouseLeftButtonDown += OnDragStart;

            this.Loaded += ComponentLoad;
            this.DataContext = this;
        }

        private void OnDragStart(object sender, MouseButtonEventArgs e)
        {
            _IsDragging = true;
            _StartPoint = e.GetPosition(this);
        }

        private void ComponentLoad(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!(VisualParent is Panel))
                return;

            _Parent = (Panel)this.VisualParent;
            _Parent.PreviewMouseMove += DraggableComponent_MouseMove;
            _Parent.PreviewMouseLeftButtonUp += (s, ex) => 
            {
                _IsDragging = false;
            };
        }

        private void DraggableComponent_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_IsDragging)
                return;

            var point = e.GetPosition(_Parent);
            double oldX = X, oldY = Y;

            X = point.X - _StartPoint.X;
            Y = point.Y - _StartPoint.Y;

            if (!_IsDragging)
                OnDrag?.Invoke(this, new DraggedEventArgs(new Point(oldX, oldY), new Point(X, Y)));
            else
            {
                var xDelta = oldX - X;
                var yDelta = oldY - Y;

                //if the draggable has moved more than one snap-thingy.
                if (Math.Sqrt((xDelta * xDelta) + (yDelta * yDelta)) >= SnapSize)
                    OnDrag?.Invoke(this, new DraggedEventArgs(new Point(oldX, oldY), new Point(X, Y)));
            }
        }
    }

    public class DraggedEventArgs
    {
        public Point OldPoint { get; set; }

        public Point NewPoint { get; set; }

        public DraggedEventArgs(Point oldPoint, Point newPoint)
        {
            OldPoint = oldPoint;
            NewPoint = newPoint;
        }
    }
}
