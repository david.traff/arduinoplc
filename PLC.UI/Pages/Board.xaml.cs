﻿using PLC.Nodes;
using PLC.UI.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PLC.UI.Pages
{
    /// <summary>
    /// Interaction logic for Board.xaml
    /// </summary>
    public partial class Board : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private Rootnode _Rootnode;
        private Rootnode Rootnode => _Rootnode;

        private double NodeSpacing = 100;

        private IEnumerable<DraggableComponent> _Draggables = new DraggableComponent[0];
        public IEnumerable<DraggableComponent> Draggables => _Draggables;

        public IEnumerable<Line> Lines => GetLines();

        private IEnumerable<Line> GetLines()
        {
            return new Line[0];
            foreach(var draggable in _Draggables)
            {
                
            }
        }

        public Board()
        {
            InitDummyData();
            LayoutComponents();
            InitializeComponent();
            this.DataContext = this;

            new Thread(() =>
            {
                while(true)
                {
                    Thread.Sleep(15);
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Notify(nameof(Lines));
                    }));
                }
            }).Start();
        }

        private void InitDummyData()
        {
            var root = Node.Create<Rootnode>();
            _Rootnode = root;
            var solver = new Nodesolver(root);

            var no = Node.Create<NONode>();
            var no1 = Node.Create<NONode>();
            var no2 = Node.Create<NONode>();


            var output = Node.Create<NONode>();
            no1.AddChild(output);
            no2.AddChild(output);
            no2.SetNodeValue(true);

            no.NodeLabel = NodeLabels.X3;
            no1.NodeLabel = NodeLabels.X1;
            no2.NodeLabel = NodeLabels.X0;
            output.NodeLabel = NodeLabels.Y0;

            no.AddChild(no1);
            no.AddChild(no2);
            no.SetNodeValue(true);

            root.AddChild(no);
        }



        private void LayoutComponents() => _Draggables = TraverseLayout(_Rootnode, new Point(50, 50));

        private IEnumerable<DraggableComponent> TraverseLayout(Node node, Point placement)
        {
            var components = new List<DraggableComponent>();
            var draggable = new RelayControl()
            {
                Node = node,
                X = placement.X,
                Y = placement.Y
            };

            components.Add(draggable);

            var idx = 0;
            foreach(var child in node.Children)
            {
                components.AddRange(TraverseLayout(child, new Point(placement.X + NodeSpacing, placement.Y + (idx++ * NodeSpacing))));
            }

            return components;
        }
    }
}
