# ArduinoPLC

A hobby-project for practicing/messing around with PLC ladder logic.
This program translates a ladder-program to Arduino-code.


### Current state:

  * Building UI for creating, editing and simulating ladder-logic programs (you are currently only able to create via code).
  * Building debugger which will run on an Arduino, which will let you view live data etc.
  

### Example:

This ladder-diagram

```
     ---| plc-enable |---| x3 |--|---| x1 |---|---( y0 )
                                 |            |
                                 |---| x2 |---|
                                 
```
Currently gives this Arduino-code as output:

```
// data-idx to label: X1 = 4, X2 = 5, X3 = 6, Y0 = 7, plc-enable = 0
bool data[5];

void setup() {
	// put your setup code here, to run once:
    pinMode(6, INPUT);
    pinMode(4, INPUT);
    pinMode(3, INPUT);

    pinMode(7, OUTPUT);


}

void loop() {
	// put your main code here, to run repeatedly:
    readPin(6, 1);    
    readPin(4, 2);    
    readPin(3, 4);    



    data[3] = data[0] && data[1] && (data[2] || data[4]);


    digitalWrite(7, data[3] ? HIGH : LOW);

}

void readPin(int pin, int dataIdx) {
	data[dataIdx] = digitalRead(pin) == HIGH;
}
```
